import 'package:analyzer/dart/element/element.dart';
import 'package:dm_gen/src/model/consumer.dart';
import 'package:dm_gen/src/model/dependency.dart';

import '../util.dart';

class ContentBuilder {
  final String name;
  final List<Dependency> dependencies;
  final List<Consumer> consumers;
  final List<ClassElement> modules;
  final List<String> imports;
  final bool nullSafety;
  final String lateSymbol;
  final String nullableSymbol;

  ContentBuilder(
    this.name,
    this.dependencies,
    this.modules,
    this.imports,
    this.consumers,
    this.nullSafety,
  )   : lateSymbol = nullSafety ? "late " : "",
        nullableSymbol = nullSafety ? "?" : "";

  String build() {
    return """
    ${initImports()}
   
    class ${name}Impl extends $name{
    ${initModule()}
    ${initDependency()}
    ${initConsumer()}
    }
  """;
  }

  String initImports() {
    final set = imports.toSet();

    for (var dependency in dependencies) {
      set.addAll(dependency.import);
    }
    for (var consumer in consumers) {
      set.addAll(consumer.import);
    }

    return set.map((item) => "import '$item';").join("\n");
  }

  String initModule() {
    var fields = "";
    var constructorParams = "";
    var defaultParams = <String>[];
    for (var module in modules) {
      fields += "final ${module.name} ${toField(module.name)};\n";
      constructorParams += "${module.name}$nullableSymbol ${toParam(module.name)},";
      defaultParams.add(
          "this.${toField(module.name)} = ${toParam(module.name)} ?? ${module.name}()");
    }
    return """
    $fields
    
      ${name}Impl(${constructorParams.isEmpty ? "" : "{$constructorParams}"})
      ${defaultParams.isEmpty ? "" : ": ${defaultParams.join(",")}"};
    """;
  }

  String initDependency() {
    var fields = "";
    var initFuture = "Future<$name> init() async {\n";
    var getters = "";

    for (var dependency in dependencies) {
      final type = dependency.typeName;
      final field = toField(dependency.typeName);
      final getter = toGetter(type);

      final builder =
          "${toField(dependency.module)}.${dependency.function}(${dependency.parameters.map((item) => "${toGetter(item)}").join(",")})";
      if (dependency.isFuture) {
        fields += "$lateSymbol$type $field;\n";
      } else if (dependency.isSingleton) {
        fields += "$type$nullableSymbol $field;\n";
      }

      if (dependency.isFuture) {
        initFuture += "$field = await $builder;\n";
        getters += "$type get $getter => $field;\n";
      } else {
        getters += "$type get $getter => ";
        if (dependency.isSingleton) {
          getters += "$field ??=";
        }
        getters += " $builder;\n";
      }
    }
    initFuture += "return this;\n";
    initFuture += "}";
    return "$fields\n\n$initFuture\n\n$getters";
  }

  String toParam(String type) {
    return "${type.firstToLower.replaceAll(RegExp("<|>|(, )"), "")}";
  }

  String toField(String type) {
    return "_${type.firstToLower.replaceAll(RegExp("<|>|(, )"), "")}";
  }

  String toGetter(String type) {
    return "_get${type.replaceAll(RegExp("<|>|(, )"), "")}";
  }

  String initConsumer() {
    String out = "";
    for (var consumer in consumers) {
      if (consumer.fromProvide) {
        out +=
            "${consumer.typeName} ${consumer.functionName}() => ${toGetter(consumer.typeName)};\n";
      } else {
        out += "${consumer.typeName} ${consumer.functionName}(";
        consumer.commonParameters.forEach((name, type) {
          out += "$type $name,";
        });
        if (consumer.optionalParameters.isNotEmpty) {
          out += "[";
          consumer.optionalParameters.forEach((name, type) {
            out += "$type $name,";
          });
          out += "]";
        }
        if (consumer.namedParameters.isNotEmpty) {
          out += "{";
          consumer.namedParameters.forEach((name, type) {
            out += "$type $name,";
          });
          out += "}";
        }

        out += ") => ";
        if (consumer.initName == null) {
          out += "null";
        } else {
          out += "${consumer.initName}(";
          final parameters = consumer.reverseParameters;

          String getParameter(String name, String type) {
            var parameter = parameters.firstWhereOrNull(
              (item) {
                return item.key == name && item.value == type;
              },
            )?.key;
            if (parameter == null) {
              parameter = parameters.firstWhereOrNull(
                (item) {
                  return item.value == type;
                },
              )?.key;
            }
            if (parameter == null) {
              parameter = toGetter(type);
            }
            return parameter;
          }

          consumer.initCommonParameters.forEach((name, type) {
            out += "${getParameter(name, type)},";
          });

          consumer.initOptionalParameters.forEach((name, type) {
            out += "${getParameter(name, type)},";
          });

          consumer.initNamedParameters.forEach((name, type) {
            out += "name: ${getParameter(name, type)},";
          });

          out += ")";
        }
        out += ";\n";
      }
    }
    return out;
  }
}
