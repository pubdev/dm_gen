import 'package:dm_gen/src/error/base_error.dart';

class ModuleIsAbstract extends BaseError {
  final String name;

  ModuleIsAbstract(this.name);

  @override
  String message() {
    return "Module $name should not be abstract";
  }
}
