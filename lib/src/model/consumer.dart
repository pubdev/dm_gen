import 'package:analyzer/dart/element/element.dart';
import 'package:dm/dm.dart';
import 'package:dm_gen/src/type_util.dart';
import 'package:dm_gen/src/util.dart';
import 'package:source_gen/source_gen.dart';

class Consumer {
  final Map<String, String> commonParameters;
  final Map<String, String> optionalParameters;
  final Map<String, String> namedParameters;

  final Map<String, String> initCommonParameters;
  final Map<String, String> initOptionalParameters;
  final Map<String, String> initNamedParameters;
  final String module;
  final String typeName;
  final String? initName;
  final String functionName;
  final Set<String> import;

  final bool fromProvide;

  List<MapEntry<String, String>> get reverseParameters =>
      (commonParameters.entries.toList()
        ..addAll(optionalParameters.entries)
        ..addAll(namedParameters.entries));

  List<String> get parameterTypes => commonParameters.values.toList()
    ..addAll(optionalParameters.values)
    ..addAll(namedParameters.values);

  List<String> get initParameterTypes => initCommonParameters.values.toList()
    ..addAll(initOptionalParameters.values)
    ..addAll(initNamedParameters.values);

  Consumer(
    this.module,
    this.typeName,
    this.initName,
    this.functionName,
    this.import,
    this.fromProvide,
    this.commonParameters,
    this.optionalParameters,
    this.namedParameters,
    this.initCommonParameters,
    this.initOptionalParameters,
    this.initNamedParameters,
  );

  static Consumer fromMethod(ClassElement module, MethodElement method,bool nullSafety) {
    final returnType = method.returnType;
    assert(!returnType.isDartAsyncFuture);
    assert(returnType.element is ClassElement);
    final fromProvide = _getIntanceChecker.hasAnnotationOf(method);

    final initCommonParameters = Map<String, String>();
    final initOptionalParameters = Map<String, String>();
    final initNamedParameters = Map<String, String>();
    final commonParameters = Map<String, String>();
    final optionalParameters = Map<String, String>();
    final namedParameters = Map<String, String>();
    final imports = <String>{};
    String? initName = returnType.getDisplayString(withNullability: nullSafety);
    imports.add(returnType.element!.library!.librarySource.uri.toString());
    getGenericTypeImports(returnType, imports);
    if (!fromProvide) {
      final constructors = (returnType.element as ClassElement).constructors;
      final constructor = constructors.firstWhereOrNull(
            (item) {
              if (_injectChecker.hasAnnotationOf(item)) {
                if (item.isPublic && !item.isAbstract) {
                  return true;
                }
                addLog(
                    "marked annotation constructor ${item.name} for $returnType is not private");
              }
              return false;
            },
          ) ??
          constructors.firstWhereOrNull(
            (item) => item.isPublic,
          );

      if (constructor == null) {
        addLog("Not have public constructor for $returnType");
        initName = null;
      } else if (constructor.name.isNotEmpty == true) {
        initName += ".${constructor.name}";
      }

      method.parameters.forEach((item) {
        imports.add(item.type.element!.library!.librarySource.uri.toString());
        getGenericTypeImports(item.type, imports);
        if (item.isNamed) {
          namedParameters[item.name] =
              item.type.getDisplayString(withNullability: nullSafety);
        } else if (item.isOptional) {
          optionalParameters[item.name] =
              item.type.getDisplayString(withNullability: nullSafety);
        } else {
          commonParameters[item.name] =
              item.type.getDisplayString(withNullability: nullSafety);
        }
      });

      constructor?.parameters.forEach((item) {
        imports.add(item.type.element!.library!.librarySource.uri.toString());
        getGenericTypeImports(item.type, imports);
        if (item.isNamed) {
          initNamedParameters[item.name] =
              item.type.getDisplayString(withNullability: nullSafety);
        } else if (item.isOptional) {
          initOptionalParameters[item.name] =
              item.type.getDisplayString(withNullability: nullSafety);
        } else {
          initCommonParameters[item.name] =
              item.type.getDisplayString(withNullability: nullSafety);
        }
      });
    }

    return Consumer(
      module.name,
      returnType.getDisplayString(withNullability: nullSafety),
      initName,
      method.name,
      imports,
      _getIntanceChecker.hasAnnotationOf(method),
      commonParameters,
      optionalParameters,
      namedParameters,
      initCommonParameters,
      initOptionalParameters,
      initNamedParameters,
    );
  }

  static const _injectChecker = TypeChecker.fromRuntime(Inject);
  static const _getIntanceChecker = TypeChecker.fromRuntime(GetInstance);
}
