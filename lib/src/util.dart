extension CaseUtil on String {
  String get firstToLower {
    if (this.isEmpty) {
      return this;
    } else {
      return this[0].toLowerCase() + this.substring(1);
    }
  }

  String get firstToUpper {
    if (this.isEmpty) {
      return this;
    } else {
      return this[0].toUpperCase() + this.substring(1);
    }
  }
}

enum Case { Upper, Lower }

String _log = "";

void addLog(String text) {
  _log += "\n\n";
  _log += text;
}

void printLog() {
  if (_log.isNotEmpty) {
    _log += "\n\n";
    print("Warning_________________________________\n" +
        _log +
        "________________________________________\n");
    _log = "";
  }
}

extension IterableUtil<E> on Iterable<E> {
  E? firstWhereOrNull(bool test(E element)) {
    for (E element in this) {
      if (test(element)) return element;
    }
    return null;
  }
}
