## [0.5.1] - 07.05.2021.

* fix for packages without null safety.

## [0.5.0] - 5.04.2021.

* update dependencies to null safety.

## [0.4.0] - 6.03.2021.

* null safety support.

## [0.3.0] - 7.10.2020.

* upgrade annotation.

## [0.2.0] - 7.10.2020.

* upgrade dependency.

## [0.1.1] - 04.04.2020.

* fix generic types.

## [0.1.0] - 29.02.2020.

* Initial release.