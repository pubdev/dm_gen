import 'package:dm/dm.dart';
import 'package:example/dependency/db.dart';

class DbModule {
  @Provide()
  Future<DB> db() => DB.init();
}
