import 'package:dm/dm.dart';
import 'package:example/dependency/cache.dart';
import 'package:example/dependency/db.dart';
import 'package:example/dependency/network.dart';

class CacheModule {
  @Provide()
  Cache cacheModule(DB db, Network<String> network) => Cache(db, network);
}
