//mock
class DB {
  DB._();

  static Future<DB> init() async {
    return DB._();
  }

  List<String> storage = [];

  Future<List<String>> getData() async {
    return storage;
  }

  Future setData(List<String> data) async {
    storage = data;
  }
}
