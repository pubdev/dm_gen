//mock
class Network<T> {
  bool enable = true;
  List<T> storage = [];

  Future<List<T>> getData() async {
    if (!enable) {
      throw Error.safeToString("disable");
    }
    return storage;
  }

  Future addData(T data) async {
    if (!enable) {
      throw Error.safeToString("disable");
    }
    storage.add(data);
  }
}
